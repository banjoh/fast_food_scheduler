package com.fastfood;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

class UnitTests {

    private Scheduler scheduler;

    @BeforeEach
    void setUp(){
        this.scheduler = new Scheduler();
    }

    @Test
    void scheduleSingleOrder() {
        this.scheduler.placeOrders(1);
        List orders = this.scheduler.getImmutableOrders();
        Assertions.assertEquals(1, orders.size());
    }

    @Test
    void scheduleManyOrders() {
        this.scheduler.placeOrders(3);
        List<Order> orders = this.scheduler.getImmutableOrders();
        Assertions.assertEquals(3, orders.size());

        Date expected = DateUtils.addSeconds(orders.get(0).getDate(), 90);
        Assertions.assertEquals(orders.get(1).getDate(), expected);

        expected = DateUtils.addSeconds(orders.get(1).getDate(), 90);
        Assertions.assertEquals(orders.get(2).getDate(), expected);
    }

    @Test
    void removeProcessedOrder() {
        this.scheduler.placeOrders(1);

        List orders = this.scheduler.getImmutableOrders();
        Assertions.assertEquals(1, orders.size());

        this.scheduler.processOrders(1);
        Assertions.assertEquals(0, orders.size());
    }

    @Test
    void scheduleAndProcessOrders() {
        this.scheduler.placeOrders(2);
        List<Order> orders = this.scheduler.getImmutableOrders();
        // First order of the day
        Assertions.assertEquals(1, orders.get(0).getOrderNumber());

        // Process and schedule some orders
        this.scheduler.processOrders(2);
        this.scheduler.placeOrders(3);
        this.scheduler.processOrders(1);

        // Check the status of the schedule
        Assertions.assertEquals(2, orders.size());
        Assertions.assertEquals(4, orders.get(0).getOrderNumber());
        Assertions.assertEquals(5, orders.get(1).getOrderNumber());
    }
}
