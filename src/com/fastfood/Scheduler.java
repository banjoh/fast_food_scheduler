package com.fastfood;

import org.apache.commons.lang3.time.DateUtils;
import java.util.*;

public class Scheduler {

    private List<Order> orders;
    private int counter;

    Scheduler() {
        this.orders = new ArrayList<>();
        this.counter = 0;
    }

    private void placeOrder() {
        Order thisOrder = new Order();
        thisOrder.setOrderNumber(++this.counter);
        if (orders.size() > 0) {
            Order newestOrder = orders.get(orders.size() - 1);
            thisOrder.setDate(DateUtils.addSeconds(newestOrder.getDate(), 90));
        }

        orders.add(thisOrder);
    }

    void placeOrders(int count) {
        for (int i = 0; i < count; i++) {
            placeOrder();
        }
    }

    void processOrders(int count) {
        for (int i = 0; i < count; i++) {
            if (orders.size() > 0) {
                orders.remove(0);
            } else break;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("   Task  - Time                          - Order No\n\n");
        for (Order order : orders) {
            sb.append(order.toString());
        }

        return sb.toString();
    }

    List<Order> getImmutableOrders() { return Collections.unmodifiableList(orders); }

    public static void main(String[] args) {
        System.out.println("Fast food schedule for today !!");
        Scheduler scheduler = new Scheduler();

        System.out.println("\nScheduled 3 orders....\n\n");
        scheduler.placeOrders(3);
        System.out.println(scheduler);

        System.out.println("\nProcessed 2 orders and scheduled 1 more....\n\n");
        scheduler.processOrders(2);
        scheduler.placeOrders(1);
        System.out.println(scheduler);

        System.out.println("\nProcessed 2 orders and scheduled 3 more....\n\n");
        scheduler.processOrders(2);
        scheduler.placeOrders(3);
        System.out.println(scheduler);
    }
}
