package com.fastfood;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class Order {
    private Date date;
    private int orderNumber;

    Order() {
        this.date = DateUtils.round(new Date(), Calendar.MINUTE);
        this.orderNumber = 1;
    }

    Date getDate() {
        return date;
    }

    int getOrderNumber() {
        return orderNumber;
    }

    void setDate(Date date) {
        this.date = date;
    }

    void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Override
    public String toString() {
        int sequenceNo = this.getOrderNumber() * 2;
        String make = String.format("%d: %s  - %s - %d\n",
                sequenceNo - 1, "Make", this.getDate(), this.getOrderNumber());
        String serve = String.format("%d: %s - %s - %d\n",
                sequenceNo, "Serve", DateUtils.addMinutes(this.getDate(), 1), this.getOrderNumber());
        return make + serve;
    }
}
