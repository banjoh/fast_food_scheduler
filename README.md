# Fast food scheduler

This is a command line application simulating how a simple task 
scheduler for preparing and serving sandwiches can work

### Dependencies
* Java 8 (e.g openjdk-8, oracle jdk-8)
* Bash environment

### Building
To build the project run the following.
```bash
./build.sh
```

All required dependencies are in the `lib` directory.This will compile 
the code and run unit tests

### Run application
Use the following command to run that application
```bash
./run.sh
```
