#!/bin/bash -e

mkdir -p bin
rm -rf bin/*

javac -d bin -cp lib/junit-jupiter-api-5.4.2.jar:lib/commons-lang3-3.9.jar -Xlint:none -sourcepath src src/com/fastfood/*.java
java -jar lib/junit-platform-console-standalone-1.4.2.jar --class-path bin:lib/commons-lang3-3.9.jar --scan-class-path
